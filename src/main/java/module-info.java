/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * This file list java modules required by Easy-installer
 * @author Vincent Bourgmayer
 * @author Ingo
 */
module ecorp.easy.installer {
    requires java.net.http;
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.base;
    requires jdk.crypto.ec; //REQUIRED TO DOWNLOAD OVER HTTPS
    requires jdk.jartool; // REQUIRED TO UNZIP DOWNLOADED ARCHIVES FOR SOME PHONES
    requires org.yaml.snakeyaml;
    requires org.slf4j;
    requires logback.classic;
    opens ecorp.easy.installer to javafx.fxml;
    opens ecorp.easy.installer.controllers to javafx.fxml;
    opens ecorp.easy.installer.controllers.subcontrollers to javafx.fxml;
    opens ecorp.easy.installer.controllers.stepControllers to javafx.fxml;
    exports ecorp.easy.installer.logger;
    exports ecorp.easy.installer;
}
