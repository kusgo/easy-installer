/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.subcontrollers;

import ecorp.easy.installer.AppConstants;
import ecorp.easy.installer.controllers.MainWindowController;
import ecorp.easy.installer.models.Phone;
import ecorp.easy.installer.tasks.UploadToEcloudTask;
import ecorp.easy.installer.utils.UiUtils;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.regex.Pattern;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * FXML Controller class
 *
 * @author Vincent Bourgmayer
 */
public class FeedbackController extends AbstractSubController {
    private final static Logger logger = LoggerFactory.getLogger(FeedbackController.class);
    private final static Pattern feelingsPattern = Pattern.compile("\nFeelings: .*");
    
    
    private @FXML Button sendAnswerBtn;
    private @FXML TextArea commentArea;
    private @FXML Label fixedFeedbackContent;
    private @FXML Label thanksLabel;
    private @FXML Label sendPrecision;
    private Button selectedFeelings;
    
    private final static String SELECTED_STYLE="selected-feelings";
    String deviceModel = "undefined";
    
    @Override
    public void setParentController(MainWindowController parentController) {
        super.setParentController(parentController); //To change body of generated methods, choose Tools | Templates.
        parentController.setNextButtonText("feedback_btn_leave");
        Phone device = parentController.getPhone();
        if(device != null){
            logger.debug("setParentController(), ParentController.getDevice() != null");
            deviceModel = device.getAdbModel()+", "+device.getAdbDevice();
        }else{
            logger.warn("setParentController(), parentController.getDevice() == null");
        }
        commentArea.setWrapText(true);
        prefillCommentArea();
    }
    
    
    /**
     * What happened when user click on a feelings button
     * @param event 
     */
    public void onFeelingsSelected(MouseEvent event){
        //When clicking a the already selected feelings, it deselect this event
        Button clickedButton = (Button) event.getSource();
        if(clickedButton.equals(selectedFeelings))  {
            
            logger.debug("onFeelingsSelected(), Deselect: {}", selectedFeelings.getId());
            selectedFeelings.getStyleClass().remove(SELECTED_STYLE);
            selectedFeelings = null;
        }
        else{
            //If another button was selected before, unselect it
            if(selectedFeelings != null) selectedFeelings.getStyleClass().remove(SELECTED_STYLE);
            
            clickedButton.getStyleClass().add(SELECTED_STYLE);
            selectedFeelings =  clickedButton;
            logger.debug("onFeelingsSelected(), select: {}", selectedFeelings.getId());
        }
        updateFeelingInText();
    }
    
    /**
     * Send the feedback to ecloud
     * @param event 
     */
    public void onSendBtnClicked(MouseEvent event){
        String filePath = createAnswerFile();
        sendAnswerBtn.setDisable(true);
        UploadToEcloudTask uploadTask = new UploadToEcloudTask(AppConstants.FEEDBACK_STORAGE_URL, filePath);
        
        uploadTask.setOnSucceeded(eh -> {
            if( (Boolean) eh.getSource().getValue() ){ //if success
                UiUtils.hideNode(sendAnswerBtn);
                UiUtils.hideNode(sendPrecision);
                UiUtils.showNode(thanksLabel);
                logger.debug("onSendBtnClicked(), sending feedback: success");
            }
            else{ //if failure
                sendAnswerBtn.setText(i18n.getString("feedback_btn_sendTryAgain"));
                sendAnswerBtn.setDisable(false);
                logger.error("onSendBtnClicked(), sending feedback: failure");
            }
        });
                
        uploadTask.setOnFailed(eh->{
            sendAnswerBtn.setDisable(false);
            sendAnswerBtn.setText(i18n.getString("feedback_btn_sendTryAgain"));
            logger.error("onSendBtnClicked(), sending feedback: failure, error: {}", eh.getSource().getException().toString());
        });
        Thread uploadTaskThread = new Thread(uploadTask);
        uploadTaskThread.setDaemon(true);
        uploadTaskThread.start();
        
    }
    
    /**
     * Prefill CommentArea with data
     */
    private void prefillCommentArea(){
        StringBuilder sb = new StringBuilder();
        String time = LocalDateTime.now().withNano(0).toString();
        
        sb.append("Timestamp: "+time.replace("T", " "))
                .append("\nDevice model: "+this.deviceModel)
                .append("\nOS name: "+AppConstants.OsName)
                .append("\nEasy-Installer version: "+AppConstants.APP_VERSION)
                .append("\nFeelings: ");
            if(selectedFeelings != null){
                sb.append(selectedFeelings.getId());
            }else{
                sb.append("not selected");
            }
        commentArea.setText("Comment: \n");
        fixedFeedbackContent.setText(sb.toString());
    }
    
    /**
     * Replace the feelings value in the commentArea
     */
    private void updateFeelingInText(){
        logger.debug("update Feeling in text");
        final String content = fixedFeedbackContent.getText();
        
        String feelings;
        if(selectedFeelings != null) feelings = selectedFeelings.getId();
        else feelings = "not selected";
        logger.debug("contain: "+feelingsPattern.matcher(content).find());
        
        final String newContent= feelingsPattern.matcher(content).replaceFirst("\nFeelings: "+feelings);
        fixedFeedbackContent.setText(newContent);
    }
    
    
    /**
     * Create the file with feedback inside
     * @return 
     */
    private String createAnswerFile(){
        String answerFilePath = AppConstants.getWritableFolder()+"feedback-"+UUID.randomUUID()+".txt";

        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(answerFilePath, true)))) {
            out.write(fixedFeedbackContent.getText()+"\n");
            out.write(commentArea.getText());
            
            out.close();
        } catch (IOException e) {
            logger.error("createAnswerFile()\n   answerFilePath = {}\n   error: {}", answerFilePath, e.toString());
         return null;
        }
        return answerFilePath;
    }
}
