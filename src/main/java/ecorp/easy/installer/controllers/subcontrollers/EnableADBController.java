/*
 * Copyright 2019-2021 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.subcontrollers;

import ecorp.easy.installer.utils.UiUtils;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * FXML Controller class
 *
 * @author Vincent
 */
public class EnableADBController extends AbstractSubSteppedController  {
    private final static Logger logger = LoggerFactory.getLogger(EnableADBController.class);

    @FXML private VBox instructionsContainer;
    @FXML private ImageView enableADBImg;
    private int stepIndex;
    private int currentInstructionIndex;
    private int imgIndex;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources); //To change body of generated methods, choose Tools | Templates.
        stepIndex =0;
        imgIndex =1;
        currentInstructionIndex = 0;
        loadStepContent(instructionsContainer.getChildren());
        updateImage();
        emphasizeLabel((Label) instructionsContainer.getChildren().get(currentInstructionIndex));
    }
    
    /**
     * Update instruction's Image
     */
    private void updateImage(){
        this.enableADBImg.setImage(loadImage("enableADB"+imgIndex+".png"));
    }
    
    /**
     * Load an image
     * @TODO: move into UiUtils
     * @param imgName
     * @return 
     */
    private Image loadImage(String imgName){
        Image image;
        try{
            image = new Image(this.getClass().getResourceAsStream("/images/"+imgName));
        }catch(Exception e){
            logger.warn("imageName = {}, error = {}", imgName, e.toString());
            image = null; 
        }
        return image;
    }
    
    /**
     * create lbl instance
     * apply style, etc...
     * @param text the text's translation key
     * @param imageName The name of the image to add as icon for the label
     * @return Label object The label instance to add into the view
     */
    private Label createInstructionLabel(String text, String imageName){
        Label result = new Label(i18n.getString(text));
        result.setWrapText(true);
        result.getStyleClass().addAll("subtitle");
        result.setGraphic(new ImageView(loadImage(imageName)));
        result.setGraphicTextGap(18); //set spacing between icon and text
        result.setContentDisplay(ContentDisplay.LEFT); //set Icon to be displayed on left
        result.getStyleClass().add("leftInsets"); //set css class to don't cover icon with emphased background
        return result;
    }
    
    /**
     * Load the content to display
     * @param instructions 
     */
    private void loadStepContent(final ObservableList<Node> instructions){
        instructions.clear();
        
        switch(stepIndex){
            case 0: //enable dev mode
                instructions.add(0, createInstructionLabel("devMode_instr_settings", "icon-gear.png"));
                instructions.add(1, createInstructionLabel("devMode_instr_build","icon-wrench.png"));     
                instructions.add(2, createInstructionLabel("devMode_instr_tap7","icon-hand.png"));     
                break;
            case 1: //enable usb debugging
                parentController.setViewTitle("debugADB_mTitle");
                instructions.add(0, createInstructionLabel("debugADB_instr_settings", "icon-gear.png"));
                instructions.add(1, createInstructionLabel("debugADB_instr_search","icon-wrench.png"));     
                instructions.add(2, createInstructionLabel("debugADB_instr_androidDebug","icon-hand.png"));     
                break;
            case 2: //enable file transfer
                parentController.setViewTitle("enableMTP_mTitle");
                instructions.add(0, createInstructionLabel("enableMTP_instr_settings", "icon-gear.png"));
                instructions.add(1, createInstructionLabel("enableMTP_instr_scrollToUSBConfig", "icon-wrench.png"));     
                instructions.add(2, createInstructionLabel("enableMTP_instr_selectMTP", "icon-hand.png"));     
                break;
            case 3: //remove all G's account
                parentController.setViewTitle("removeAccounts_mTitle");
                instructions.add(0, createInstructionLabel("debugADB_instr_settings", "icon-gear.png"));
                instructions.add(1, createInstructionLabel("removeAccounts_instr_openAccounts", "icon-wrench.png"));
                instructions.add(2, createInstructionLabel("removeAccounts_instr_selectAccounts", "icon-hand.png"));
                break;
            default:
                break;
        }
    }
    
    @Override
    protected void onNextButtonClicked() {
        final ObservableList<Node> instructions = instructionsContainer.getChildren();
        
        deemphasizeLabel((Label) instructions.get(currentInstructionIndex));
        ++imgIndex;
       
        if(currentInstructionIndex == instructions.size()-1){
            currentInstructionIndex = 0;
                ++stepIndex;
                
                UiUtils.playFadeAnimation(instructionsContainer.getParent(), 
                e->{ //Load new content
                    loadStepContent(instructions); 
                    emphasizeLabel((Label) instructions.get(currentInstructionIndex));
                    updateImage();},
                e->{});
        }else{
            if(stepIndex == 3 && currentInstructionIndex == instructions.size()-2){ //at last step, move to device detection
                parentController.resetNextButtonEventHandler();
            }
            ++currentInstructionIndex; 
            // The two below lines can't be put at the end of the method
            // because of FadeAnimation below
            emphasizeLabel((Label) instructions.get(currentInstructionIndex));
            updateImage();
        }
    }
}