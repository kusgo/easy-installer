/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.subcontrollers;

import ecorp.easy.installer.controllers.MainWindowController;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

/**
 * @TODO: may be find a better name
 * @author vincent
 */
public abstract class AbstractSubSteppedController extends AbstractSubController{
    
    protected final String  currentSubStepCssClass = "currentSubStep";
    protected int currentSubStepId =0;
    
    /**
     * Override setParentController, to set the nextButton on click Listener
     * @param parentController the parent controller
     */
    @Override
    public void setParentController(MainWindowController parentController){
        super.setParentController(parentController);
        setNextButtonOnClickListener();
    }
    
    protected void setNextButtonOnClickListener(){
        parentController.setNextButtonOnClickListener( ( MouseEvent event) -> {
            if(event.getEventType().equals(MouseEvent.MOUSE_CLICKED)){
                onNextButtonClicked();
            }
            ++currentSubStepId;
        });
    }
    
    /**
     * Remove the currentSubStepCssClass from the given label
     * @param label 
     */
    protected void deemphasizeLabel(Label label){
        label.getStyleClass().remove(currentSubStepCssClass);
    }
    
    /**
     * Add the currentSubStepCssClass to the given label
     * @param label 
     */
    protected void emphasizeLabel(Label label){
        label.getStyleClass().add(currentSubStepCssClass);
    }
    
    /**
     * Set the new behaviour of the nextButton when it is clicked.
     * 
     * /!\ In many case, don't forget to reset it to normal behaviour /!\
     * 
     * /!\ currentSubStepId is increase by 1 just after the call of this method /!\
     */
    protected abstract void onNextButtonClicked();
}
