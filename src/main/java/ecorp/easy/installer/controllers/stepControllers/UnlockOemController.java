/*
 * Copyright 2019-2021 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.controllers.stepControllers;


import ecorp.easy.installer.graphics.FlashGlobalProgressManager;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.HBox;

/**
 * This controller is for UI about unlocking OEM on the stock ROM
 * It's main goal is to allow to get extra code from website or API
 * 
 * @author vincent Bourgmayer
 */
public class UnlockOemController extends StepController{
    
    @FXML Label instruction;
    @FXML Hyperlink link;
    @FXML HBox globalProgressIndicator;
    private FlashGlobalProgressManager globalProgressMgr;
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources){
        super.initialize(location, resources);
        link.setBorder(Border.EMPTY);
        
        parentController.setNextButtonVisible(true);   
        globalProgressMgr = new FlashGlobalProgressManager( parentController.getPhone().getFlashingProcess().getStepsCount() );
        globalProgressIndicator.getChildren().addAll(globalProgressMgr.getSegments());
        globalProgressMgr.updateProgression();
    }
    
    public void openUrl(){
        parentController.openUrlInBrowser(link.getText());
    }
}
