/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.graphics;

import java.util.ArrayList;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author vincent Bourgmayer
 */
public class FlashGlobalProgressManager {
    private final static String COLOR_CURRENT_SEGMENT = "#1789E9"; //blue
    private final static String COLOR_PENDING_SEGMENT = "#E5E5E5"; //grey
    private final static String COLOR_SUCCEED_SEGMENT = "#1EBE54"; //green
    
    int stepNumber;
    ArrayList<Rectangle> segments;
    int currentStepId; //store the index of the segment of the current step
    
    public FlashGlobalProgressManager(int stepNumber){
        this.stepNumber = stepNumber;
        segments = new ArrayList<>();
        currentStepId = -1;
        initSegments();
    }
    
    /**
     * Add as many segments as stepNumber
     */
    private void initSegments(){
        for(int i =0; i < stepNumber;i++){
            Rectangle segment = new Rectangle();
            segment.setWidth(118.0);
            segment.setHeight(6.0);
            segment.setFill(Paint.valueOf(COLOR_PENDING_SEGMENT));

            segments.add( segment );
        }
    }

    /**
     * Update Progression to the specified step
     * @return 
     */
    public boolean updateProgression(){
        if(currentStepId >= 0){
            //Replace the currentStep style of the segment which borrow it by a success style
            segments.get(currentStepId).setFill(Paint.valueOf(COLOR_SUCCEED_SEGMENT));
        }
        
        final int nextId = currentStepId+1;
        if(nextId< stepNumber){
            
            //add the current style to the current new current segment
            segments.get(nextId).setFill(Paint.valueOf(COLOR_CURRENT_SEGMENT));
            currentStepId = nextId;
        }
        return true;
    }
    
    /**
     * Return the array with the different segments
     * @return 
     */
    public ArrayList<Rectangle> getSegments(){
        return segments;
    }
}
