/*
 * Copyright 2019-2020 - ECORP SAS 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.tasks;

import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import ecorp.easy.installer.AppConstants;
import javafx.concurrent.Task;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author vincent Bourgmayer
 */
public class AskAccountTask extends Task<String>{
    private final static Logger logger = LoggerFactory.getLogger(AskAccountTask.class);
    String email;
    String sugar;
    /**
     * 
     * @param mail the mail used to ask account
     * @param sugar Sugar element's used to build one parameter of the POST REQUEST
     */
    public AskAccountTask(String mail, String sugar){
        email = mail;//URLEncoder.encode(mail, StandardCharsets.UTF_8); //encoding here result in failure
        this.sugar = sugar;//URLEncoder.encode(sugar, StandardCharsets.UTF_8);
    }
    
    /**
     * @inheritDoc 
     * @return String object
     * @throws Exception 
     */
    @Override
    protected String call() throws Exception {
        HttpClient client = HttpClient.newHttpClient();
        
        Map<Object, Object> rawDatas = new HashMap<>();
        rawDatas.put("email", email );
        rawDatas.put("check", buildCheckData());      
        
        HttpRequest request = buildPostRequest(buildFormDataFromMap(rawDatas));
        
        HttpResponse<String> response = client.send(request,
        HttpResponse.BodyHandlers.ofString());
        
        logger.debug("call(), response.body() = {}",response.body() );
        return response.body();
    }
    
    
    /**
     * format the datas send as param of Post request
     * source: https://mkyong.com/java/java-11-httpclient-examples/
     * @param data unformated data
     * @return  formated data
     */
    private HttpRequest.BodyPublisher buildFormDataFromMap(Map<Object, Object> data) {
        var builder = new StringBuilder();
        for (Map.Entry<Object, Object> entry : data.entrySet()) {
            if (builder.length() > 0) {
                builder.append("&");
            }
            builder.append(URLEncoder.encode(entry.getKey().toString(), StandardCharsets.UTF_8));
            builder.append("=");
            builder.append(entry.getValue().toString()); //entry value is already encoded
        }
        return HttpRequest.BodyPublishers.ofString(builder.toString());
    }
    
    /**
     * Build the POST request
     * @param datas
     * @return 
     */
    private HttpRequest buildPostRequest(HttpRequest.BodyPublisher datas){
        HttpRequest request = HttpRequest.newBuilder()
            .POST(datas)
            .setHeader("User-Agent", "Java 11 HttpClient Bot") // add request header
            .header("Content-Type", "application/x-www-form-urlencoded")
            .uri(URI.create(AppConstants.MURENA_BASE_URL + "/signup/process_email_invite.php"))
            .build();
        
        return request;
    }
    
    /**
     * create the param's value for "check"
     * @return String
     */
    private String buildCheckData() throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update((email+sugar).getBytes());
        byte[] digest = md.digest();
        
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < digest.length; i++) {
          sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}