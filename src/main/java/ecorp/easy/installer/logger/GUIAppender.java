/*
 * Copyright 2019-2020 - ECORP SAS

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.logger;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import java.util.ArrayList;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 *
 * @author vincent
 */
public class GUIAppender extends AppenderBase<ILoggingEvent>{
    private ArrayList<Text> logsList= new ArrayList<>();
    private TextFlow container;

    @Override
    protected void append(ILoggingEvent e) {
        if(logsList.size() > 100){
            logsList.remove(0);
        }
        Text txt = buildLabel(e);
        logsList.add(txt);
        
        //side effect
        displayLog(txt);
    }
    
    public synchronized void setContainer(TextFlow container){
        this.container = container;
    }
    
    
    private synchronized void displayLog(Text text){
        if(container != null){
            container.getChildren().add(text);
        }
    }
    
    private Text buildLabel(ILoggingEvent e){
        Text result = new Text(e.getFormattedMessage()+"\n");
        return result;
    }
    
    public ArrayList<Text> getLogsList() {
        return logsList;
    }
}