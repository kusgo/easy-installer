/*
 * Copyright 2019-2022 - ECORP SAS 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer;
import java.nio.file.FileSystems;
import java.io.File;
import java.nio.file.Paths;

/**
 * @TODO make all final static field with uppercase or lowcase but not a mix of both
 * @author Vincent Bourgmayer
 * @author Omer Akram
 * @author Frank Preel
 */
public abstract class AppConstants {


    public final static String APP_VERSION = "v0.20";
    public final static String Separator = FileSystems.getDefault().getSeparator();
    public final static String OsName = System.getProperty("os.name");
    public final static String JavaHome = System.getProperty("java.home");
    public final static String SourcesFolderName = "sources";
    public final static String ScriptsFolderName = "scripts";
    public final static String HeimdallFolderName = "heimdall";

    public final static String AdbFolderName = "adb";

    public final static String MURENA_BASE_URL = "https://murena.io";

    public final static String FEEDBACK_STORAGE_URL = MURENA_BASE_URL + "/s/QLwyiZ4fysodiz3";
    public final static String LOG_STORAGE_URL = MURENA_BASE_URL + "/s/4qRxWjeM5Yb72b4";
    private final static String BuildSrcFolderName = "buildSrc";

    //step type key
    public final static String USER_ACTION_KEY="action";
    public final static String LOAD_KEY="load";
    public final static String ASK_ACCOUNT_KEY="askAccount";
    public final static String UNLOCK_OEM_KEY="enableOemUnlock";
    
    
     // Not fully constructed
    private static String TWRP_IMAGE_PATH;
    private static String E_ARCHIVE_PATH;
    private static String DEVICE_MODEL;
    private static String PATCH_PATH;

    /**
     * Get the path of patch
     * @return can return null if not already setted
     */
    public static String getPatchPath() {
        return PATCH_PATH;
    }

    /**
     * Define the path to access patch to flash
     * @param eImagePath
     */
    public static void setPatchPath(String patchPath) {
        PATCH_PATH = patchPath;
    }
        
    /**
     * This methods set the model of the device
     * @todo remove this
     * @param deviceModel 
     */
    public static void setDeviceModel(String deviceModel){
        DEVICE_MODEL = deviceModel;
    }

    /**
     * This methods get the model of the device
     */
    public static String getDeviceModel(){
        return DEVICE_MODEL;
    }

    /**
     * Get the path to eel OS archive to flash
     * @return can return null if not already setted
     */
    public static String getEArchivePath() {
        return E_ARCHIVE_PATH;
    }

    /**
     * Define the path to access eel OS Archive to flash
     * @param eImagePath
     */
    public static void setEArchivePath(String eImagePath) {
        E_ARCHIVE_PATH = eImagePath;
    }

    /**
     * Get path of TWRP archive to use
     * @return can return null if not setted
     */
    public static String getTwrpImgPath(){
        return TWRP_IMAGE_PATH;
    }

    /**
     * Define where TWRP archive will be store
     * @param twrpImgPath path to archive
     */
    public static void setTwrpImgPath(String twrpImgPath){
        TWRP_IMAGE_PATH = twrpImgPath;
    }
    
    /**
     * Get path to the folder where the app download source to flash the device
     * @return 
     */
    public static String getSourcesFolderPath(){

        String path = getWritableFolder()+SourcesFolderName+Separator;
        
        if(DEVICE_MODEL != null){
            path+=DEVICE_MODEL+Separator;
        }
        System.out.println("path = "+path);
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
        return path;
    }
    
    /**
     * Get path to the folder where scripts are stored
     * @return 
     */
    public static String getScriptsFolderPath(){
        if (System.getProperty("IDE", null) != null) {
            String osPath = "";
            if (isWindowsOs()) {
                osPath = "windows";
            } else if (isLinuxOs() || OsName.toLowerCase().contains("mac") ) {
                osPath = "linux";
            }
            return getRootPath() + "flash-scripts" + Separator + osPath + Separator;
        }
        return getRootPath()+ScriptsFolderName+Separator;
    }
    
    /**
     * Get path to the folder which contain adb standalone
     * @return 
     */
    public static String getADBFolderPath(){
        if (System.getProperty("IDE", null) != null) {
            String osPath = "";
            if (isWindowsOs()) {
                osPath = "windows";
            } else if (OsName.toLowerCase().contains("mac")) {
                osPath = "OSX";
            } else if (isLinuxOs()) {
                osPath = "linux";
            }
            return getRootPath() + BuildSrcFolderName  + Separator + osPath + Separator + AdbFolderName + Separator;
        }
        return getRootPath()+AdbFolderName+Separator;
    }
    
        /**
     * Get path to the folder which contain heimdall standalone
     * @return
     */
    public static String getHeimdallFolderPath(){
        if (System.getProperty("IDE", null) != null) {
            String osPath = null;
            if (isWindowsOs()) {
                osPath = "windows";
            } else if (OsName.toLowerCase().contains("mac")) {
                osPath = "OSX";
            } else if (isLinuxOs()) {
                osPath = "linux";
            }
            return getRootPath() + BuildSrcFolderName + Separator + osPath + Separator + HeimdallFolderName + Separator;
        } 
        return getRootPath();

    }
    
    /**
     * Get the root path of the running app.
     * Based on "JavaHome/bin/"
     * @return 
     */
    public static String getRootPath(){
    	String rootPath = JavaHome+Separator+"bin"+Separator;
    	if(System.getProperty("IDE", null) != null) {
            rootPath =  Paths.get("").toAbsolutePath().toString()+Separator;
        }
		if (rootPath.indexOf(" ")>0){ //K1ZFP set quote to full path
			rootPath = "\"" + rootPath + "\"";
		}
		return rootPath;
    }

    /**
     * Indicate if the app run from a snap
     * [Linux only]
     * @return 
     */
    private static boolean isSnap(){
        // The name "easy-installer" must match the name of the snap inside
        // snapcraft.yaml.
        return System.getenv("SNAP_NAME") != null && System.getenv("SNAP_NAME").equals("easy-installer");
    }
    
     /**
     * Return the path to a foldre that the app can write in
     * @return 
     */
    public static String getWritableFolder(){
        // Snap packages are read-only by nature, hence we can't
        // really write to "java.home" directory. This codepath
        // detects if we are running from a snap runtime and
        // returns a path that is specific to snap and is writable.
        if (isLinuxOs()){
            if(isSnap()){
                return System.getenv("SNAP_USER_COMMON")+Separator;
            }else{
                return "/tmp/easy-installer"+Separator;
            }
        }else if(isWindowsOs()){
            return System.getenv("localappdata")+Separator+"easy-installer"+Separator;
        }else
            return getRootPath();
        
    }
    
    /**
     * Tell if the current OS is linux/unix
     * @return true if linux or unix
     */
    public static boolean isLinuxOs(){
        return OsName.contains("nix") || OsName.contains("nux");
    }
    
    /**
     * Tell if the current OS is linux/unix
     * @return true if linux or unix
     */
    public static boolean isWindowsOs(){
        return OsName.toLowerCase().contains("win");
    }
}
