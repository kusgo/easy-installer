/*
 * Copyright 2019-2020 - ECORP SAS 

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.models;

import ecorp.easy.installer.models.steps.IStep;
import java.util.HashMap;


/**
 * This class encapsulate the tree of steps to flash a device
 * Could be name: StepTree or StepMap or FlashingProcess
 * @author Vincent Bourgmayer
 */
public class Process {

    private final int stepsCount; //Number of step to consider
    private HashMap<String, IStep> steps; //Map of steps to follow

    /**
     * Constructor of ProcessMould
     * Create an empty list of steps
     * @param stepCount int number of step used for FlashProcess
     */
    public Process(int stepCount){
        this.stepsCount = stepCount;
        this.steps = new HashMap<>();
    }
    
    /**
     * Get the name of the phone's model
     * @return 
     */
    public int getStepsCount() {
        return stepsCount;
    }

    /**
     * Get the list of steps in this processMould
     * @return 
     */
    public HashMap<String, IStep> getSteps() {
        return steps;
    }

    /**
     * Replace current steps by the new one
     * @param steps the new steps
     */
    public void setSteps(HashMap<String, IStep> steps) {
        this.steps = steps;
    }
    
    /**
     * Add a step object
     * @param key
     * @param step 
     */
    public void addStep(String key, IStep step){
        this.steps.put(key, step);
    }
}