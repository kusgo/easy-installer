/*
 * Copyright 2021 - ECORP SAS 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ecorp.easy.installer.utils;

import ecorp.easy.installer.models.steps.IStep;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * What to test: Mainly parsing result
 * Try to use invalid type. By example:
 * Trying to parse data as String instead of int
 * 
 * Question: Do I need to parse a real config file
 * or can I just create Map instance with data ?
 * @author vincent Bourgmayer
 */
public class ConfigParserTest {
    
    public ConfigParserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of parseStep method, of class ConfigParser.
     */
    @Test
    public void testParseStepWithNullYaml() throws Exception {
        System.out.println("testParseStepWithNullYaml");
        Map yaml = null;
        IStep result = null;
        try{
            result = ConfigParser.parseStep(yaml);
        }catch(Exception e){
            assertEquals("Exception isn't the one expected", NullPointerException.class, e.getClass());
        }
        assertNull("No Step should be returned", result);
    }
    
    /**
     * Assert that parsing fail and return expected Exception in case of invalid
     * data type
     */
    @Test
    public void testParseStepWithInvalidDataType() throws Exception {
        System.out.println("testParseStepWithInvalidDataType");
        IStep result = null;
        
        Map yaml = new HashMap<>();
        yaml.put("type", "basic");
        yaml.put("nextStepKey", "f1");
        yaml.put("stepNumber", "10");
        
        try{
            result = ConfigParser.parseStep(yaml);
        }catch(Exception e){
            assertEquals("#1 ClassCastException was expected but got "+e.getClass().getSimpleName(), ClassCastException.class, e.getClass());
        }
        
        assertNull("No Step should be returned", result);
        
        System.out.println("parseStep");
        yaml = new HashMap<>();
        yaml.put("type", "basic");
        yaml.put("nextStepKey", 5);
        yaml.put("stepNumber", 1);
        
        try{
            result = ConfigParser.parseStep(yaml);
        }catch(Exception e){
            assertEquals("#2 ClassCastException was expected but got "+e.getClass().getSimpleName(), ClassCastException.class, e.getClass());
        }
        assertNull("No Step should be returned", result);
    }
    
    @Test
    public void testParseStepWithMissingIntData() throws Exception {
        System.out.println("testParseStepWithMissingIntData");
        IStep result = null;
        
        Map yaml = new HashMap<>();
        yaml.put("type", "basic");
        yaml.put("nextStepKey", "f1");
        
        try{
            result = ConfigParser.parseStep(yaml);
        }catch(Exception e){
            assertEquals("NullPointerException was expected but got "+e.getClass().getSimpleName(), NullPointerException.class, e.getClass());
        }
        
        assertNull("No Step should be returned", result);
    }
    
    @Test
    public void testParseStepWithMissingStringData() throws Exception {
        System.out.println("testParseStepWithMissingIntData");
        IStep result = null;
        
        Map yaml = new HashMap<>();
        yaml.put("type", "basic");
        yaml.put("stepNumber", 1);
        
        try{
            result = ConfigParser.parseStep(yaml);
        }catch(Exception e){
            fail("No Exception expected");
        }
        
        assertNotNull(" Step should be returned", result);
        assertNull("IStep.getNextStepKey() should return null", result.getNextStepKey());
    }
    
    @Test
    public void testParseStepWithMissingStepType() throws Exception {
        System.out.println("testParseStepWithMissingIntData");
        IStep result = null;
        
        Map yaml = new HashMap<>();
        yaml.put("stepNumber", 1);
        yaml.put("nextStepKey", "f1");
        
        try{
            result = ConfigParser.parseStep(yaml);
        }catch(Exception e){
            assertEquals("NullPointerException was expected but got "+e.getClass().getSimpleName(), NullPointerException.class, e.getClass());
        }
        
        assertNull(" Step should be null", result);
    }
}
