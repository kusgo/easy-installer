#!/bin/bash

# Copyright (C) 2020 - Author: Ingo
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# TODO: What if 2 devices detected?
# Parameter
# $1: The folder where fastboot runnable is stored
# $2: The archive folder path
# $3: THe model of the device

# Exit status
# - 0 : Device in fastboot mode detected and bootloader unlocked
# - 1 : Error

FASTBOOT_FOLDER_PATH=$1
FASTBOOT_PATH=${FASTBOOT_FOLDER_PATH}"fastboot"

echo "fastboot path: $FASTBOOT_PATH"

ARCHIVE_PATH=$2
ARCHIVE_FOLDER_PATH=$(dirname "$2")"/"

echo "Archive Path="$ARCHIVE_FOLDER_PATH

device_model=$3

echo "Model="$device_model

SECURITY_PATCH=${ARCHIVE_FOLDER_PATH}""${device_model}"-security-patch"
ORIGINAL_SECURITY_PATCH=$(cat "$SECURITY_PATCH")
MURENA_ROM_INFO=${ARCHIVE_FOLDER_PATH}""${device_model}"-rom-info"
MURENA__SECURITY_PATCH=`sed -n 's/^ro.build.version.security_patch=//p' $MURENA_ROM_INFO`
echo "MURENA__SECURITY_PATCH=$MURENA__SECURITY_PATCH"

# Assuming format is xxxx-yy-zz with otional extra info ..
function versionToInt { printf "%03d%03d%03d%03d" $(echo "$1" | tr '-' ' '); }

I_ORIGINAL_SECURITY_PATCH=$(versionToInt "$ORIGINAL_SECURITY_PATCH")
I_MURENA__SECURITY_PATCH=$(versionToInt $MURENA__SECURITY_PATCH)

if [[ "$I_ORIGINAL_SECURITY_PATCH" -lt "1" ]]
then
    echo "ORIGINAL ROM INFO NOT AVAILABLE => DO NOT PROCESS"
    exit 0
elif [[ $I_MURENA__SECURITY_PATCH -ge $I_ORIGINAL_SECURITY_PATCH ]]
then
    echo "GREATER OR EQUALS => PROCESS"
else
    echo "LOWER => DO NOT PROCESS"
    exit 0
fi

while [ "$($FASTBOOT_PATH getvar unlocked 2>&1 | grep -q "unlocked: no"; echo $?)" = 1 ]
do
  sleep 2
done

#sleep 5
echo "fastboot mode detected, phone is unlocked"
