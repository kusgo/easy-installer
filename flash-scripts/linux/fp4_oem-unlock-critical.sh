#!/bin/bash

# Copyright (C) 2020 - Author: Ingo; update and adoption to FP4, 2022 by ff2u
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Parameter
# $1: The folder where fastboot runnable is stored

# Exit status
# - 0 : OEM unlocked on device
# - 10 : fastboot error

FASTBOOT_FOLDER_PATH=$1
FASTBOOT_PATH=${FASTBOOT_FOLDER_PATH}"fastboot"

echo "fastboot path: $FASTBOOT_PATH"

if [ "$($FASTBOOT_PATH oem device-info 2>&1 | grep -q "critical unlocked: true"; echo $?)" = 0 ]
then
    sleep 10
    echo "Device already critically unlocked!"
else
    if [ "$($FASTBOOT_PATH flashing unlock_critical; echo $?)" = 1 ]
    then
        echo "Crtical unlock fails!"
        exit 10
    fi
    sleep 10
    echo "Critical unlocked!"
fi
