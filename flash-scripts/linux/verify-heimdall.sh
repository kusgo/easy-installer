#!/bin/bash

# Copyright (C) 2019 ECORP SAS - Author: Romain Hunault
# Copyright (C) 2020 ECORP SAS - Author: Vincent Bourgmayer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Parameter
# $1: The folder where heimdall runnable is stored

# Exit status
# - 0 : OEM unlocked on device
# - 10 : heimdall error

HEIMDALL_FOLDER_PATH=$1
HEIMDALL_PATH=${HEIMDALL_FOLDER_PATH}"heimdall"

echo "Heimdall path: $HEIMDALL_PATH"




if [ "$($HEIMDALL_PATH print-pit)" = 1 ]
then
	echo "OEM-unlock fails"
  	exit 10
fi

sleep 5
echo "OEM unlocked"