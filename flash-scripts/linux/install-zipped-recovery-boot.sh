#!/bin/bash

# Copyright (C) 2023 Murena SAS - Author: SahilSonar
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Parameter
# $1: TWRP_IMAGE_PATH need twrp path (${TWRP_FOLDER}/${TWRP})
# $2: FASTBOOT_FOLDER_PATH needs fastboot binary path
# $3: JAVA_FOLDER_PATH needs java binary path

# Exit status
# - 0 : Recovery installed
# - 101 : TWRP_IMAGE_PATH missing
# - 103 : Failed to unzip

TWRP_IMAGE_PATH=$1
OUTPUT_FOLDER=$(dirname $TWRP_IMAGE_PATH)
FASTBOOT_FOLDER_PATH=$2
FASTBOOT_PATH=${FASTBOOT_FOLDER_PATH}"fastboot"
JAVA_FOLDER_PATH=$3
JAR_PATH=${JAVA_FOLDER_PATH}"/bin/jar"

echo "fastboot path: $FASTBOOT_PATH"

if [ -z "$TWRP_IMAGE_PATH" ]
then
	echo "TWRP Image path is empty"
  	exit 101
fi

# Delete any existing *.img files in OUTPUT_FOLDER
rm -rf "$OUTPUT_FOLDER"/*.img
rm -rf "$OUTPUT_FOLDER"/*.img.sha256sum

# Use jar tool to extract all .img files from the zip archive
cd "$OUTPUT_FOLDER" || exit 102
if ! "$JAR_PATH" -x -v -f "$TWRP_IMAGE_PATH" ;
then
  exit 103
fi

echo "Flashing boot $DEVICE_ID"
"$FASTBOOT_PATH" flash boot "$OUTPUT_FOLDER"/recovery-*-*.img

echo "Flashing dtbo $DEVICE_ID"
"$FASTBOOT_PATH" flash dtbo "$OUTPUT_FOLDER"/dtbo-*-*.img