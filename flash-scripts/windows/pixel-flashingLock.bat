@echo off

:: Coyright (C) 2023 ECORP SAS - Author: Frank Preel

:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.

:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.

:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: device id
:: $2: fastboot folder path
:: $3: image to flash

:: Exit status
:: - 0 : bootloader locked
:: - 1 : unknown error
:: - 2 : Flashing unlocked failed
:: - 101 : $DEVICE_ID missing
:: - 102 : $FASTBOOT_FOLDER_PATH is missing
:: - 103 : $E_IMAGE_PATH is missing


SET DEVICE_ID=%~1
SET FASTBOOT_FOLDER_PATH=%~2
SET E_IMAGE_PATH=%~3

IF not defined %DEVICE_ID (
  exit /b 101
)
IF not defined %FASTBOOT_FOLDER_PATH (
  exit /b 102
)
IF not defined %E_IMAGE_PATH (
  echo "E Image path is empty"
  exit /b 103
)

SET FASTBOOT_PATH="%FASTBOOT_FOLDER_PATH%fastboot"

%FASTBOOT_PATH% erase avb_custom_key
%FASTBOOT_PATH% flash avb_custom_key "%E_IMAGE_PATH%"

%FASTBOOT_PATH% getvar unlocked 2>&1 | findstr "yes"
if  %errorlevel% == 0 (
	echo "The device is unlocked"
	%FASTBOOT_PATH% -s %DEVICE_ID% flashing lock
) else (
	exit /b 0
)

:fastboot_detect
%FASTBOOT_PATH% getvar unlocked 2>&1 | findstr "no"
if errorLevel 1 (
	echo "Wait lock"
	timeout 2 >nul 2>&1
	goto :fastboot_detect
) 

call fastboot_detect

exit /b 0
