@echo off

:: Copyright (C) 2022 ECORP SAS - Author: Frank Preel

:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.

:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.

:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: DEVICE_ID device id
:: $2: ARCHIVE_PATH path to archive
:: $3: fastboot folder path


:: Exit status
:: - 0 : device flashed
:: - 1 : generic error

SET DEVICE_ID=%~1
SET ARCHIVE_FILE=%~2
SET FASTBOOT_FOLDER_PATH=%~3

SET ADB_CMD="%FASTBOOT_FOLDER_PATH%adb"
%ADB_CMD% -s %DEVICE_ID% "sideload" "%ARCHIVE_FILE%"
if errorlevel==0 (
  echo "Sideload OK"
  exit /b 0
)
if errorlevel==1 (
  echo "Sideload OK"
  exit /b 0
) 
echo "Sideload fails"
exit /b 1
