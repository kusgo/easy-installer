@echo off

:: !/bin/bash

::  Copyright (C) 2023 Murena SAS - Author: SahilSonar

::  This program is free software: you can redistribute it and/or modify
::  it under the terms of the GNU General Public License as published by
::  the Free Software Foundation, either version 3 of the License, or
::  (at your option) any later version.

::  This program is distributed in the hope that it will be useful,
::  but WITHOUT ANY WARRANTY; without even the implied warranty of
::  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
::  GNU General Public License for more details.

::  You should have received a copy of the GNU General Public License
::  along with this program.  If not, see <https://www.gnu.org/licenses/>.

::  Parameter
::  $1: TWRP_IMAGE_PATH need twrp path (${TWRP_FOLDER}/${TWRP})
::  $2: FASTBOOT_FOLDER_PATH needs fastboot binary path 
::  $3: JAVA_FOLDER_PATH needs java binary path
::  Exit status
::  - 0 : Recovery installed
::  - 101 : TWRP_IMAGE_PATH missing
::  - 103 : Failed to unzip

SET TWRP_IMAGE_PATH=%1
SET OUTPUT_FOLDER=%~dp1
SET FASTBOOT_FOLDER_PATH=%2
SET FASTBOOT_PATH=%FASTBOOT_FOLDER_PATH%fastboot
SET JAVA_FOLDER_PATH=%3
SET JAR_PATH=%JAVA_FOLDER_PATH%\bin\jar

echo "fastboot path: %FASTBOOT_PATH%"

IF not defined TWRP_IMAGE_PATH (
  echo "TWRP Image path is empty"
  exit /b 101
)

:: Delete any existing *.img files in OUTPUT_FOLDER
del /Q %OUTPUT_FOLDER%\*.img
del /Q %OUTPUT_FOLDER%\*.img.sha256sum

:: Use jar tool to extract all .img files from the zip archive
cd %OUTPUT_FOLDER% || exit /b 102
%JAR_PATH% -x -v -f "%TWRP_IMAGE_PATH%"

IF %ERRORLEVEL% neq 0 (
  exit /b 103
)

echo "Flashing boot"
for %%I in (%OUTPUT_FOLDER%\boot-*-*.img) do %FASTBOOT_PATH% flash boot "%%I"

echo "Flashing dtbo"
for %%I in (%OUTPUT_FOLDER%\dtbo-*-*.img) do %FASTBOOT_PATH% flash dtbo "%%I"

echo "Flash the recovery"
for %%I in (%OUTPUT_FOLDER%\recovery-*-*.img) do %FASTBOOT_PATH% flash vendor_boot "%%I"
