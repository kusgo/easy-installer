:: Copyright (C) 2021 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: DEVICE_ID device id
:: $2: ARCHIVE_PATH path to archive
:: $3: fastboot folder path
:: $4: Java folder path


:: Exit status
:: - 0 : device flashed
:: - 1 : generic error
:: - 10: can't unpack system.img
:: - 11: can't wipe data
:: - 12: can't flash system
:: - 13: can't flash vendor
:: - 14: can't flash boot
:: - 15: can't flash dtbo
:: - 16: can't flash recovery
:: - 17: can't flash logo
:: - 18: can't flash md1dsp
:: - 19: can't flash md1img
:: - 20: can't flash spmfw
:: - 21: can't flash lk
:: - 22: can't flash lk2
:: - 23: can't flash sspm_1
:: - 24: can't flash sspm_2
:: - 25: can't flash tee1
:: - 26: can't flash tee2
:: - 27: can't flash preloader
:: - 101 : DEVICE_ID missing
:: - 102 : ARCHIVE_PATH missing
:: - 103 : fastboot folder path missing

set DEVICE_ID="%1"
set ARCHIVE_PATH=%~2
set FASTBOOT_FOLDER_PATH=%~3
set JAVA_FOLDER_PATH=%~4

if not defined %DEVICE_ID (
  exit /b 101
)

if not defined %ARCHIVE_PATH (
  exit /b 102
)

if not defined %FASTBOOT_FOLDER_PATH (
  exit /b 103
)

:: Check java folder has been provided
if not defined %JAVA_FOLDER_PATH (
  exit /b 104
)

set FASTBOOT_PATH="%FASTBOOT_FOLDER_PATH%fastboot"


set JAR_PATH="%JAVA_FOLDER_PATH%/bin/jar"

:: Build archive folder path
for %%a in ("%ARCHIVE_PATH%") do (
	set ARCHIVE_FOLDER_PATH=%%~dpa"
	echo %ARCHIVE_FOLDER_PATH%
)
:: unzip all images
cd "%ARCHIVE_FOLDER_PATH%"
%JAR_PATH% -x -v -f "%ARCHIVE_PATH%"
if errorLevel 1 ( exit /b 10 )
echo "unpacked archive"
timeout 1 >nul 2>&1

%FASTBOOT_PATH% -w
if errorLevel 1 ( exit /b 11 )
echo "user data wiped"
timeout 5 >nul 2>&1

:: Flash the device
%FASTBOOT_PATH% -s %DEVICE_ID% flash system system.img
if errorLevel 1 ( exit /b 12 )
timeout 1 >nul 2>&1
echo "Flashed system"

%FASTBOOT_PATH% -s %DEVICE_ID% flash vendor vendor.img
if errorLevel 1 ( exit /b 13 )
timeout 1 >nul 2>&1
echo "Flashed vendor"

%FASTBOOT_PATH% -s %DEVICE_ID% flash boot boot.img
if errorLevel 1 ( exit /b 14 )
timeout 1 >nul 2>&1
echo "Flashed boot"

%FASTBOOT_PATH% -s %DEVICE_ID% flash dtbo dtbo.img
if errorLevel 1 ( exit /b 15 )
timeout 1 >nul 2>&1
echo "Flashed dtbo"

%FASTBOOT_PATH% -s %DEVICE_ID% flash recovery recovery.img
if errorLevel 1 ( exit /b 16 )
timeout 1 >nul 2>&1
echo "Flashed recovery"

%FASTBOOT_PATH% -s %DEVICE_ID% flash logo logo.img
if errorLevel 1 ( exit /b 17 )
timeout 1 >nul 2>&1
echo "Flashed logo"

%FASTBOOT_PATH% -s %DEVICE_ID% flash md1dsp md1dsp.img
if errorLevel 1 ( exit /b 18 )
timeout 1 >nul 2>&1
echo "Flashed md1dsp"

%FASTBOOT_PATH% -s %DEVICE_ID% flash md1img md1img.img
if errorLevel 1 ( exit /b 19 )
timeout 1 >nul 2>&1
echo "Flashed md1img"

%FASTBOOT_PATH% -s %DEVICE_ID% flash spmfw spmfw.img
if errorLevel 1 ( exit /b 20 )
timeout 1 >nul 2>&1
echo "Flashed spmfw"

%FASTBOOT_PATH% -s %DEVICE_ID% flash lk lk.img
if errorLevel 1 ( exit /b 21 )
timeout 1 >nul 2>&1
echo "Flashed lk"

%FASTBOOT_PATH% -s %DEVICE_ID% flash lk2 lk.img
if errorLevel 1 ( exit /b 22 )
timeout 1 >nul 2>&1
echo "Flashed lk2"

%FASTBOOT_PATH% -s %DEVICE_ID% flash sspm_1 sspm.img
if errorLevel 1 ( exit /b 23 )
timeout 1 >nul 2>&1
echo "Flashed sspm_1"

%FASTBOOT_PATH% -s %DEVICE_ID% flash sspm_2 sspm.img
if errorLevel 1 ( exit /b 24 )
timeout 1 >nul 2>&1
echo "Flashed sspm_2"

%FASTBOOT_PATH% -s %DEVICE_ID% flash tee1 trustzone1.bin
if errorLevel 1 ( exit /b 25 )
timeout 1 >nul 2>&1
echo "Flashed tee1"

%FASTBOOT_PATH% -s %DEVICE_ID% flash tee2 trustzone2.bin
if errorLevel 1 ( exit /b 26 )
timeout 1 >nul 2>&1
echo "Flashed tee2"

%FASTBOOT_PATH% -s %DEVICE_ID% flash preloader preloader.img
if errorLevel 1 ( exit /b 27 )
timeout 1 >nul 2>&1
echo "Flashed preloader"

exit /b 0
