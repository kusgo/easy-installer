:: Copyright (C) 2020 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: TODO: What if 2 devices detected?
:: $1: The folder where heimdall runnable is stored
:: Exit status
:: - 0 : Device in download mode detected
:: - 1 : Error

set HEIMDALL_FOLDER=%~1
if not defined %HEIMDALL_FOLDER (EXIT /b 1)
set HEIMDALL_PATH="%HEIMDALL_FOLDER%heimdall"

:heimdall_detect
%HEIMDALL_PATH% detect >nul
if errorLevel 1 (
	echo not found
	ping 127.0.0.1 -n 2 -w 1000 >NUL
	goto :heimdall_detect
) else (echo succeed) 
EXIT /B 0

call heimdall_detect

ping 127.0.0.1 -n 2 -w 5000 >NUL