@echo off

:: !/bin/bash

::  Copyright (C) 2022-2023 ECORP SAS - Author: Frank Preel

::  This program is free software: you can redistribute it and/or modify
::  it under the terms of the GNU General Public License as published by
::  the Free Software Foundation, either version 3 of the License, or
::  (at your option) any later version.

::  This program is distributed in the hope that it will be useful,
::  but WITHOUT ANY WARRANTY; without even the implied warranty of
::  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
::  GNU General Public License for more details.

::  You should have received a copy of the GNU General Public License
::  along with this program.  If not, see <https://www.gnu.org/licenses/>.

::  Parameter
::  $1: FASTBOOT_PATH 
::  $2: E_IMAGE_PATH need twrp path 
::  Exit status
::  - 0 : Recovery installed
::  - 101 : E_IMAGE_PATH missing

SET E_IMAGE_PATH=%~1
SET FASTBOOT_PATH=%~2

IF not defined %E_IMAGE_PATH (
  echo "E Image path is empty"
  exit /b 101
)
SET FASTBOOT_CMD="%FASTBOOT_PATH%fastboot"

%FASTBOOT_CMD% flash vendor_boot %E_IMAGE_PATH%
