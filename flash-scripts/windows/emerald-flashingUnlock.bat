@echo off

:: Coyright (C) 2022 ECORP SAS - Author: Frank Preel

:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.

:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.

:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: device id
:: $2: fastboot folder path

:: Exit status
:: - 0 : bootloader locked
:: - 1 : unknown error
:: - 2 : Flashing unlocked failed
:: - 101 : $DEVICE_ID missing
:: - 102 : $FASTBOOT_FOLDER_PATH is missing


SET DEVICE_ID=%~1
SET FASTBOOT_FOLDER_PATH=%~2

IF not defined %DEVICE_ID (
  exit /b 101
)
IF not defined %FASTBOOT_FOLDER_PATH (
  exit /b 102
)

SET FASTBOOT_PATH="%FASTBOOT_FOLDER_PATH%fastboot"
%FASTBOOT_PATH% -s %DEVICE_ID% flashing unlock
if errorlevel 1 ( 
	exit /b 2 
)

timeout 1 >nul 2>&1
exit /b 0
