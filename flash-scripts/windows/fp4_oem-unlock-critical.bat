:: Copyright (C) 2020 - Author: Ingo; update and adoption to FP4, 2022 by ff2u
:: Copyright (C) 2020 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: The folder where fastboot runnable is stored

:: Exit status
:: - 0 : OEM unlocked on device
:: - 10 : fastboot error

set FASTBOOT_FOLDER_PATH=%~1
set FASTBOOT_PATH="%FASTBOOT_FOLDER_PATH%fastboot"

echo "FASTBOOT path:"%FASTBOOT_PATH%

%FASTBOOT_PATH% oem device-info 2>&1 | findstr /c:"critical unlocked: true"
if %ERRORLEVEL% EQU 0 (
    waitfor /t 10 pause 2>nul
    echo "Device already critically unlocked!"
) else (
    %FASTBOOT_PATH% flashing unlock_critical
    if errorLevel 1 (
      echo "Critical unlock fails!"
      exit /b 10
    )
    waitfor /t 10 pause 2>nul
    echo "Critical unlocked!"
)

exit /b 0
