:: Copyright (C) 2020 - Author: Ingo
:: Copyright (C) 2020 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: The folder where fastboot runnable is stored

:: Exit status
:: - 0 : OEM unlocked on device
:: - 10 : fastboot error

set FASTBOOT_FOLDER_PATH=%~1
set FASTBOOT_PATH="%FASTBOOT_FOLDER_PATH%fastboot"
echo "FASTBOOT path:"%FASTBOOT_PATH%

%FASTBOOT_PATH% oem unlock

if errorLevel 1 (
  echo "OEM-unlock fails"
  exit /b 10
)

timeout 10 >nul
echo "OEM unlocked"

exit /b 0