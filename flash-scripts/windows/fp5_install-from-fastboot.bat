:: Copyright (C) 2020 - Author: Ingo; update and adoption to FP4, 2022 by ff2u
:: Copyright (C) 2020 ECORP SAS - Author: Vincent Bourgmayer
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.

:: Parameter
:: $1: ARCHIVE_PATH path to the /e/ archive to flash
:: $2: FASTBOOT_FOLDER_PATH: the path where runnable fastboot is stored
:: $3: JAVA_FOLDER_PATH: the path where runnable jar is stored (to unpack ZIP file)

:: Exit status
:: - 0 : /e/ installed
:: - 1 : erasing userdata or metadata failed
:: - 2 : flashing of a non-critical partition failed
:: - 4 : setting active slot to a failed
:: - 5 : flashing of a critical partition failed
::       namely: {devcfg_ab, xbl_ab, tz_ab, hyp_ab, keymaster_ab, abl_ab, aop_ab, imagefv_ab, multiimgoem_ab, qupfw_ab, uefisecapp_ab, xbl_config_ab}
:: - 101 : ARCHIVE_PATH missing
:: - 102 : archive could not be unpacked

set ARCHIVE_PATH=%~1
set FASTBOOT_FOLDER_PATH=%~2
set FASTBOOT_PATH="%FASTBOOT_FOLDER_PATH%fastboot"
set JAVA_FOLDER_PATH=%~3
set JAR_PATH="%JAVA_FOLDER_PATH%\bin\jar"

if not defined %ARCHIVE_PATH (
  exit /b 101
)

for %%a in ("%ARCHIVE_PATH%") do (
	set ARCHIVE_NAME="%%~na"
)

for %%a in ("%ARCHIVE_PATH%") do (
	set ARCHIVE_FOLDER_PATH="%%~dpa"
)

echo "archive path : "%ARCHIVE_PATH%
echo "archive folder path : "%ARCHIVE_FOLDER_PATH%
echo "fastboot path : "%FASTBOOT_PATH%
echo "jar path : "%JAR_PATH%

cd "%ARCHIVE_FOLDER_PATH%"
waitfor /t 1 pause 2>nul

%JAR_PATH% -x -v -f "%ARCHIVE_PATH%"
if errorLevel 1 ( exit /b 102 )
echo "unpacked archive"
waitfor /t 1 pause 2>nul

echo "=== Flash slot a ==="

%FASTBOOT_PATH% flash bluetooth_a bluetooth.img
if errorLevel 1 ( exit /b 2 )
echo "flashed bluetooth_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash devcfg_a devcfg.img
if errorLevel 1 ( exit /b 5 )
echo "flashed devcfg_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash dsp_a dsp.img
if errorLevel 1 ( exit /b 2 )
echo "flashed dsp_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash modem_a modem.img
if errorLevel 1 ( exit /b 2 )
echo "flashed modem_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash xbl_a xbl.img
if errorLevel 1 ( exit /b 5 )
echo "flashed xbl_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash tz_a tz.img
if errorLevel 1 ( exit /b 5 )
echo "flashed tz_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash hyp_a hyp.img
if errorLevel 1 ( exit /b 5 )
echo "flashed hyp_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash keymaster_a keymaster.img
if errorLevel 1 ( exit /b 5 )
echo "flashed keymaster_a"
waitfor /t 1 pause 2>nul


%FASTBOOT_PATH% flash abl_a abl.img
if errorLevel 1 ( exit /b 5 )
echo "flashed abl_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash boot_a boot.img
if errorLevel 1 ( exit /b 2 )
echo "flashed boot_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash dtbo_a dtbo.img
if errorLevel 1 ( exit /b 2 )
echo "flashed dtbo"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash vendor_boot_a vendor_boot.img
if errorLevel 1 ( exit /b 2 )
echo "flashed vendor_boot_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash vbmeta_system_a vbmeta_system.img
if errorLevel 1 ( exit /b 2 )
echo "flashed vbmeta_system_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash vbmeta_a vbmeta.img
if errorLevel 1 ( exit /b 2 )
echo "flashed vbmeta_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash super super.img
if errorLevel 1 ( exit /b 2 )
echo "flashed super"
waitfor /t 1 pause 2>nul


%FASTBOOT_PATH% flash aop_a aop.img
if errorLevel 1 ( exit /b 5 )
echo "flashed aop_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash featenabler_a featenabler.img
if errorLevel 1 ( exit /b 2 )
echo "flashed featenabler_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash imagefv_a imagefv.img
if errorLevel 1 ( exit /b 5 )
echo "flashed imagefv_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash multiimgoem_a multiimgoem.img
if errorLevel 1 ( exit /b 5 )
echo "flashed multiimgoem_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash qupfw_a qupfw.img
if errorLevel 1 ( exit /b 5 )
echo "flashed qupfw_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash uefisecapp_a uefisecapp.img
if errorLevel 1 ( exit /b 5 )
echo "flashed uefisecapp_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash xbl_config_a xbl_config.img
if errorLevel 1 ( exit /b 5 )
echo "flashed xbl_config_a"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash cpucp_a cpucp.img
if errorLevel 1 ( exit /b 2 )
echo "flashed cpucp_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash shrm_a shrm.img
if errorLevel 1 ( exit /b 2 )
echo "flashed shrm_b"
waitfor /t 1 pause 2>nul


echo "=== Flash slot b ==="

%FASTBOOT_PATH% flash bluetooth_b bluetooth.img
if errorLevel 1 ( exit /b 2 )
echo "flashed bluetooth_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash devcfg_b devcfg.img
if errorLevel 1 ( exit /b 5 )
echo "flashed devcfg_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash dsp_b dsp.img
if errorLevel 1 ( exit /b 2 )
echo "flashed dsp_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash modem_b modem.img
if errorLevel 1 ( exit /b 2 )
echo "flashed modem_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash xbl_b xbl.img
if errorLevel 1 ( exit /b 5 )
echo "flashed xbl_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash tz_b tz.img
if errorLevel 1 ( exit /b 5 )
echo "flashed tz_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash hyp_b hyp.img
if errorLevel 1 ( exit /b 5 )
echo "flashed hyp_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash keymaster_b keymaster.img
if errorLevel 1 ( exit /b 5 )
echo "flashed keymaster_b"
waitfor /t 1 pause 2>nul


%FASTBOOT_PATH% flash abl_b abl.img
if errorLevel 1 ( exit /b 5 )
echo "flashed abl_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash boot_b boot.img
if errorLevel 1 ( exit /b 2 )
echo "flashed boot_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash dtbo_b dtbo.img
if errorLevel 1 ( exit /b 2 )
echo "flashed dtbo_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash vendor_boot_b vendor_boot.img
if errorLevel 1 ( exit /b 2 )
echo "flashed vendor_boot_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash vbmeta_system_b vbmeta_system.img
if errorLevel 1 ( exit /b 2 )
echo "flashed vbmeta_system_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash vbmeta_b vbmeta.img
if errorLevel 1 ( exit /b 2 )
echo "flashed vbmeta_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash aop_b aop.img
if errorLevel 1 ( exit /b 5 )
echo "flashed aop_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash featenabler_b featenabler.img
if errorLevel 1 ( exit /b 2 )
echo "flashed featenabler_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash imagefv_b imagefv.img
if errorLevel 1 ( exit /b 5 )
echo "flashed imagefv_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash multiimgoem_b multiimgoem.img
if errorLevel 1 ( exit /b 5 )
echo "flashed multiimgoem_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash qupfw_b qupfw.img
if errorLevel 1 ( exit /b 5 )
echo "flashed qupfw_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash uefisecapp_b uefisecapp.img
if errorLevel 1 ( exit /b 5 )
echo "flashed uefisecapp_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash xbl_config_b xbl_config.img
if errorLevel 1 ( exit /b 5 )
echo "flashed xbl_config_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash cpucp_b cpucp.img
if errorLevel 1 ( exit /b 2 )
echo "flashed cpucp_b"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% flash shrm_b shrm.img
if errorLevel 1 ( exit /b 2 )
echo "flashed shrm_b"
waitfor /t 1 pause 2>nul

echo "=== Wiping userdata and metadata ==="
waitfor /t 3 pause 2>nul

%FASTBOOT_PATH% erase userdata
if errorLevel 1 ( exit /b 1 )
echo "wiped userdata"
waitfor /t 1 pause 2>nul

%FASTBOOT_PATH% erase metadata
if errorLevel 1 ( exit /b 1 )
echo "wiped metadata"
waitfor /t 3 pause 2>nul

echo "=== Setting active slot to a ==="
%FASTBOOT_PATH% --set-active=a
if errorLevel 1 ( exit /b 4 )
echo "switched slot to a"
waitfor /t 1 pause 2>nul

exit /b 0
