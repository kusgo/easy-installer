:: Copyright (C) 2022 - Author: Frank
::
:: This program is free software: you can redistribute it and/or modify
:: it under the terms of the GNU General Public License as published by
:: the Free Software Foundation, either version 3 of the License, or
:: (at your option) any later version.
::
:: This program is distributed in the hope that it will be useful,
:: but WITHOUT ANY WARRANTY; without even the implied warranty of
:: MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
:: GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License
:: along with this program.  If not, see <https://www.gnu.org/licenses/>.
::
:: TODO: What if 2 devices detected?
:: Parameter
:: $1: The folder where fastboot runnable is stored
:: $2: The archive folder 
:: $3: The device model
::
:: Exit status
:: - 0 : Device in fastboot mode detected
:: - 1 : Error (adb)
:: - 2 : Error (device locked)

:: Store ROM information on the file system 

@echo OFF



set FASTBOOT_FOLDER_PATH=%~1
set ADB_PATH="%FASTBOOT_FOLDER_PATH%adb"

echo "adb command=%ADB_PATH%""

set ARCHIVE_PATH=%~2
echo "Archive Path=%ARCHIVE_PATH%"
for %%a in ("%ARCHIVE_PATH%") do (
	set ARCHIVE_FOLDER_PATH="%%~dpa
)

echo "Archive Folder Path="%ARCHIVE_FOLDER_PATH%

set device_model=%~3

echo "Model=%device_model%"

%ADB_PATH% shell getprop ro.build.version.security_patch

if errorLevel 1 (
	echo "adb error (1)"
    exit /b 1
)

set SECURITY_PATCH=%ARCHIVE_FOLDER_PATH%%device_model%-security-patch"
set DEVICE_STATE=%ARCHIVE_FOLDER_PATH%%device_model%-device-state"
set MURENA_ROM_INFO=%ARCHIVE_FOLDER_PATH%%device_model%-rom-info"

bitsadmin.exe /transfer "RomInfo" https://images.ecloud.global/stable/FP4/e-latest-FP4.zip.prop %MURENA_ROM_INFO% > nul

:: For FP4 on stock ROM the key [ro.build.flavor] returns [qssi-user].
:: On Murena OS: returns [lineage_FP4-userdebug]
:: This seems to be the most sane way to assert if we are on stock ROM
copy /y NUL %SECURITY_PATCH% >NUL 

%ADB_PATH% shell getprop ro.build.flavor 2>&1 | findstr  "qssi-user"
if errorLevel 1 (
	::We are NOT on a stock ROM let's assume the job (unlock) is done and continue the process.
	echo "Custom ROM case"
	exit /b 0
)

echo "Stock ROM"
%ADB_PATH% shell  getprop ro.boot.vbmeta.device_state 2>&1 | findstr "unlocked"
if errorLevel 1 (
	echo "The device is locked"
	%ADB_PATH% shell getprop ro.build.version.security_patch> %SECURITY_PATCH%
)

exit /b 0

