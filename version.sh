#!/bin/bash -e

JAVA_VERSION_FILE=${JAVA_VERSION_FILE:-src/main/java/ecorp/easy/installer/AppConstants.java}
SNAPCRAFT_VERSION_FILE=${SNAPCRAFT_VERSION_FILE:-snap/snapcraft.yaml}
WINDOWS_VERSION_FILE=${WINDOWS_VERSION_FILE:-windows-installer-mui.nsi}
AUR_VERSION_FILE=${AUR_VERSION_FILE:-pkg/arch/PKGBUILD}
MACOS_VERSION_FILE=${MACOS_VERSION_FILE:-Mac-build/Info.plist}

function check() {
  JAVA_VERSION=$(cat $JAVA_VERSION_FILE | grep "APP_VERSION" | sed -E 's/.*"(v.*)".*/\1/')
  echo "Java version: $JAVA_VERSION ($JAVA_VERSION_FILE)"

  SNAPCRAFT_VERSION=$(cat $SNAPCRAFT_VERSION_FILE | grep "^version" | sed -E "s/^version: '(v.*)'.*/\1/")
  echo "Snapcraft version: $SNAPCRAFT_VERSION ($SNAPCRAFT_VERSION_FILE)"

  WINDOWS_VERSION=$(cat $WINDOWS_VERSION_FILE | grep "define appVersion" | sed -E 's/!define appVersion "(v.*)"/\1/')
  echo "Windows version: $WINDOWS_VERSION ($WINDOWS_VERSION_FILE)"

  AUR_VERSION=$(cat $AUR_VERSION_FILE | grep "^pkgver" | sed 's/[^=]*= *//;s/.*/v&/')
  echo "AUR version: $AUR_VERSION ($AUR_VERSION_FILE)"

  MACOS_VERSION=$(cat $MACOS_VERSION_FILE | grep -A 1 CFBundleGetInfoString | tail -n 1 | sed -E "s/<string>(.*)<\/string>/v\1/" | xargs)
  echo "MACOS version: $MACOS_VERSION ($MACOS_VERSION_FILE)"

  echo "Tag version: $CI_COMMIT_TAG"

  if [ "$JAVA_VERSION" == "$CI_COMMIT_TAG" ] && [ "$SNAPCRAFT_VERSION" == "$CI_COMMIT_TAG" ] && [ "$WINDOWS_VERSION" == "$CI_COMMIT_TAG" ] && [ "$AUR_VERSION" == "$CI_COMMIT_TAG" ] && [ "$MACOS_VERSION" == "$CI_COMMIT_TAG" ]
  then
    exit 0
  fi
}

function bump() {
  local old=$1
  local new=$2
  sed -i 's!'$old'!'$new'!g' $JAVA_VERSION_FILE $SNAPCRAFT_VERSION_FILE $WINDOWS_VERSION_FILE $AUR_VERSION_FILE $MACOS_VERSION_FILE
  exit 0
}

if [ "$1" == "check" ]; then
  # CI_COMMIT_TAG=v0.15.2 ./version.sh check
  check
elif [ "$1" == "bump" ] && [ "$#" == "3" ]; then
  # ./version.sh bump 0.15.2 0.16
  bump $2 $3
fi

exit 1
