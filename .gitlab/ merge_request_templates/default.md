## Description

// Describe what the MR bring

## Issue(s)
Closes #

## Test environment
- [ ] non regression test

/!\ __Checked OS should at least be tested:__ /!\
- [ ] windows
- [ ] linux 
- [ ] MacOS _Not supported yet__

/!\ __Checked devices should at least be tested:__ /!\
_If no device checked, then you can pick anyone from the list_

- [ ] GS290
- [ ] FP3
- [ ] 2e
- [ ] starlte 
- [ ] star2lte
- [ ] dreamlte 
- [ ] dream2lte
- [ ] herolte 
- [ ] hero2lte


__HOW TO SETUP__

- Download build from latest pipeline of this MR

### windows
 - Extracted  Installer from the build downloaded
 - Double click on it
 - Follow the setup wizard
 - Start from shorcut in menu

### linux with Snap
- You need to have snap available from your shell. 

  - Debian/ubuntu based : 
    ```
    sudo apt install snapd
    ```
  - Fedora based : 
    ```
    sudo dnf install snapd
    ```
  - Arch linux based :
    ```
    yaourt -S snapd
    sudo systemctl enable --now snapd.socket
    ```
- Extract snap file to some location
- Open shell prompt and move to this location (using cd <your location>)
- run : `sudo snap install --dangerous <yourFile.snap>`. _Note: I have a doubt about sudo_
- run : `snap connect easy-installer:adb-support`
- run : `sudo snap connect easy-installer:raw-usb`

### Uninstall the easy-installer test build:
#### Windows:
- Open menu 
- Right click on easy-installer
- select uninstall

Or remove it like any other software from uninstall settings menu.

#### Linux:
- run `sudo snap remove easy-installer` in a shell

## Test procedures

//Describe the process to Follow

//Describe what should be checked

## Screenshots
// Screenshots which can help reviewer

## Check list
- [ ] Self review
- [ ] Test procedure explained
- [ ] Tested on fresh install
- [ ] Tested applied as an update
- [ ] License
- [ ] Internal documentation
- [ ] User documentation
